﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Movie.Models;
using Movie.Repository;

namespace Movie.Controllers
{
    public class TitleController : Controller
    {
        
        TitleRepo titleRepo = new TitleRepo();

        /// <summary>
        /// Get the list of titles
        /// </summary>
        /// <returns></returns>
        public ActionResult Titles()
        {
            var titles = new MoviesList();
            titles.MovieBasicsList = titleRepo.GetMovies();
            return View(titles);
        }

        /// <summary>
        /// Get the detail description for selected title.
        /// </summary>
        /// <param name="titleId"></param>
        /// <returns></returns>
        public ActionResult TitleDescription(long titleId)
        {
            var titleDescription = new TitleDescriptionList();
            titleDescription.TitleDescriptions = titleRepo.GetMovieDetails(titleId);
            return View("TitleDetails", titleDescription);
        }

    }
}
