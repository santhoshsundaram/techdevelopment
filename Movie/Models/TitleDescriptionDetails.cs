﻿using System;
using System.Collections.Generic;
using System.Data.Objects.DataClasses;
using System.Linq;
using System.Web;

namespace Movie.Models
{
    /// <summary>
    /// This class contains list of title description.
    /// </summary>
    public class TitleDescriptionList
    {
        public TitleDescriptionList()
        {
            TitleDescriptions = new List<TitleDescriptionDetails>();
        }

        public List<TitleDescriptionDetails> TitleDescriptions {get; set;}
    }

    /// <summary>
    /// This class contains detail description of title.
    /// </summary>
    public class TitleDescriptionDetails
    {
        public List<Award> Awards { get; set; }
        public List<StoryLine> StoryLines { get; set; }
        public List<OtherName> OtherNames { get; set; }
    }
}