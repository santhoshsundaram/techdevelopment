﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Movie.Models
{
    /// <summary>
    /// This class contains list of title.
    /// </summary>
    public class MoviesList
    {
        public MoviesList()
        {
            MovieBasicsList = new List<TitleList>();
        }

        public List<TitleList> MovieBasicsList { get; set; }
    }

    /// <summary>
    /// This class contains list of title with basic details. 
    /// </summary>
    public class TitleList
    {
        public int TitleId { get; set; }
        public string MovieName { get; set; }
        public int? ReleaseYear { get; set; }
        public string AwardWon { get; set; }
        public string GenreName { get; set; }
        public bool IsOnScreen { get; set; }

    }
}