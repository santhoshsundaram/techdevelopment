﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Movie.Models;

namespace Movie.Repository
{
    public class TitleRepo
    {
        public List<TitleList> GetMovies()
        {
            List<TitleList> movieInfo = null;
                using (var movieEntities = new TitlesEntities())
                {
                    movieInfo = (from movieinfo in movieEntities.Titles
                                 select new TitleList
                                 {
                                     TitleId = movieinfo.TitleId,
                                     MovieName = movieinfo.TitleName,
                                     AwardWon = movieinfo.Awards.Count() > 0 ? "Yes" : "No",
                                     GenreName = movieinfo.TitleGenres.Count() > 1 ? "Multiple" : "Single",
                                     ReleaseYear = movieinfo.ReleaseYear,
                                     IsOnScreen = movieinfo.TitleParticipants.FirstOrDefault().IsOnScreen,
                                 }).Distinct().ToList();

                }
            return movieInfo;

        }

        public List<TitleDescriptionDetails> GetMovieDetails(long titleId)
        {
            List<TitleDescriptionDetails> titleDescriptionDetails = null;
            using (var movieEntities = new TitlesEntities())
            {
                titleDescriptionDetails = (from movieinfo in movieEntities.Titles.AsEnumerable()
                             where movieinfo.TitleId == titleId
                             select new TitleDescriptionDetails 
                             {
                                 Awards = movieinfo.Awards.ToList(),
                                 StoryLines = movieinfo.StoryLines.ToList(),
                                 OtherNames = movieinfo.OtherNames.ToList(),
                             }).Distinct().ToList();
            }
            return titleDescriptionDetails;
        }
    }

     


}