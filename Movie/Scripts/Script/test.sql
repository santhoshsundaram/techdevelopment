USE [Promosuite]
GO
if not exists(select * from sys.indexes
where name = 'NCI_RateCardIdRateTime' and object_id = (select object_id from sys.objects where name = 'tblRateCardDetail' ))
begin
	CREATE NONCLUSTERED INDEX [NCI_RateCardIdRateTime]
	ON [dbo].[tblRateCardDetail] ([RateCardId],[RateTime])
	INCLUDE ([MondayRate],[TuedayRate],[WednesdayRate],[ThursdayRate],[FridayRate],[SaturdayRate],[SundayRate])
end
GO
if not exists(select * from sys.indexes
where name = 'NCI_NWFeedCDAsrunDay' and object_id = (select object_id from sys.objects where name = 'DT_COPY_ASRUN' ))
begin
	CREATE NONCLUSTERED INDEX [NCI_NWFeedCDAsrunDay]
	ON [dbo].[DT_COPY_ASRUN] ([NETWORK_FEED_CD],[ASRUN_DAY])
	INCLUDE ([ISCI_CD],[ASRUN_TIME],[AIRING_DATETM],[TITLE],[COB_FLAG],[CONTENT_ID],[FRANCHISE],[INVENTORY_NAME])
end
GO